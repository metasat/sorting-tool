# coding: utf-8
import json

# this script removes the orig and the orphans
# it can also remove notes if needed

infile = "export.json.txt"

outfile = "mission"

# comment out if you want the notes
# uncomment to remove notes
notes = "no"

########

json_file= open(infile,"r")
data = json.load(json_file)


#print data

content = data["children"][0]["children"]


#function from https://stackoverflow.com/a/54109459
def delete_keys_from_dict(d, to_delete):
    if isinstance(to_delete, str):
        to_delete = [to_delete]
    if isinstance(d, dict):
        for single_to_delete in set(to_delete):
            if single_to_delete in d:
                del d[single_to_delete]
        for k, v in d.items():
            delete_keys_from_dict(v, to_delete)
    elif isinstance(d, list):
        for i in d:
            delete_keys_from_dict(i, to_delete)
    return d

try:
    if notes == "no":
        content = delete_keys_from_dict(content,"note")
except:
    pass

with open(outfile+".json", "wb") as out:
    json.dump(content, out)