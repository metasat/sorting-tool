import json
import csv


# originating CSV and text file with notes should have the same
# filename (other than extension).  Update this variable to that
# file name. Output json will have the same filename.

filename = "star_reorg"

# note file format, can include html:
# concept|note
# i.e.
# Observation Vetted Status|Good: There is signal of the satellite/ transmitter;<br/>Bad: There isn't signal of the satellite/transmitter;<br/>Failed: Station failed to perform an observation or upload its results;<br/>Unknown: Not yet vetted.
# GS Status|Online: the station is connected with the network and can perform observations;<br/>Testing: Same with the online but stil needs configuration or other tweaks before it goes online;<br/>Offline: Station is not connected to network.



########

f = open(filename+".txt", "r").read()

line = f.splitlines()

concepts = {}
concept_list = []

for x in line:
    y = x.split("|")
    print y[0]
    concepts[y[0]] = y[1]
    concept_list.append(y[0])


c = []

with open(filename+".csv", 'rb') as csvfile:
      reader = csv.reader(csvfile)
      for row in reader:
        c.append(row)

def createJSON(data):

  j = {}

  def appendLeaf(row, j):

    if not len(row) or not row[0]:
      return

    #it's already there, just go down a level
    for c in j.setdefault("children", []):
      if c["name"] == row[0]:
        appendLeaf(row[1:], c)
        return

    #it's not there, add it
    if row[0] in concept_list:
        j["children"].append({"name":row[0],"note":concepts[row[0]]})
    else:
        j["children"].append({"name" : row[0]})

    appendLeaf(row[1:], j["children"][-1])

  for row in data:
    appendLeaf(row,j)

  with open(filename+".json", "wb") as out:
    json.dump(j["children"][0], out)


createJSON(c[1:])